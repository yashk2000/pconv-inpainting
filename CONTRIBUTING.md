# CONTRIBUTING GUIDELINES

This documentation contains a set of guidelines to help you during the contribution process. 
We are happy to welcome all the contributions from anyone willing to improve/add new functionalities to this project. Thank you for helping out and remember, **no contribution is too small.**

# Submitting Contributions👩‍💻👨‍💻
Below you will find the process and workflow used to review and merge your changes.

## Step 0 : Find an issue
- Take a look at the Existing Issues or create your **own** Issues!
- Wait for the Issue to be assigned to you after which you can start working on it.
- Note: Every change in this project should/must have an associated issue.

## Step 1 : Fork the Project
- Fork this Repository. This will create a Local Copy of this Repository on your GitLab Profile. Keep a reference to the original project in `upstream` remote.
```bash
$ git clone https://gitlab.com/<your user name>/pconv-inpainting.git
$ cd pconv-inpainting
$ git remote add upstream https://gitlab.com/yashk2000/pconv-inpainting.git
```

- If you have already forked the project, update your copy before working.
```
$ git remote update
$ git checkout <branch-name>
$ git rebase upstream/main
```

## Step 2 : Branch
Create a new branch. Use its name to identify the issue your addressing.
```
# It will create a new branch with name Branch_Name and switch to that branch 
$ git checkout -b branch_name
```
## Step 3 : Work on the issue assigned
- Work on the issue(s) assigned to you. 
- Add all the files/folders needed.
- After you've made changes or made your contribution to the project add changes to the branch you've just created by:
```
# To add all new files to branch Branch_Name
$ git add .
```

## Step 4 : Commit
- To commit give a descriptive message for the convenience of reveiwer by:
```
# This message get associated with all files you have changed
$ git commit -m 'message'
```

### Commit style guidelines

- Machine Leanring: If you're working on improving the jupyter notebook, or the part directly related to the inpainting model, the commit should begin with `model: Brief Description`
- Desktop App: If you're working on implementing a feature in the frontend, the commit should begin with `frontend: 'Brief Description'`
- General: If you're working on documentation, or any other chore, your commit should begin with `general: 'Brief Description'`

## Step 5 : Work Remotely
- Now you are ready to your work to the remote repository.
- When your work is ready and complies with the project conventions, upload your changes to your fork:

```
# To push your work to your remote repository
$ git push -u origin Branch_Name
```

## Step 6 : Pull Request
- Go to your repository in browser and click on compare and merge requests. Then add a title and description to your pull request that explains your contribution.
- Voila! Your Merge Request has been submitted and will be reviewed by the moderators and merged.🥳

## Tip from us😇
It always takes time to understand and learn. So, do not worry at all. We know **you have got this**!💪
